"""
helper functions for home_inventory
"""
import pandas as pd
import numpy as np

from dash.exceptions import PreventUpdate

from decorators import print_func_name


def inventory_to_table(items):
    items = items if isinstance(items, list) else [items]
    out = []
    for j, i in enumerate(items):
        i = i.copy()
        # split tags when storing in table with pipe |
        i['Tags'] = "|".join(i["Tags"]) if isinstance(i["Tags"], list) else i["Tags"]
        out += [pd.DataFrame(i, index=[j])]
    out = pd.concat(out)
    return out


def create_nested_dict(l, d=None):
    """
    create a nested dict with the end of the list being the lowest level
    if list is empty return
    - this is used to create a nested location dict
    """

    if d is None:
        d = {}
    if len(l) == 0:
        return d
    else:
        d[l[0]] = {}
        create_nested_dict(l[1:], d[l[0]])
    return d


@print_func_name
def get_location_list(items):

    loc_dict = {}
    for id, item in items.items():
        # find the highest location that is not in the location dict
        for i, j in enumerate(item['Location']):
            if j not in loc_dict:
                break

        # if there are not any locations, continue
        if len(item['Location']) == 0:
            continue
        # if the first location was not found, add entirely
        if i == 0:
            loc_dict[item['Location'][i]] = create_nested_dict(item['Location'][(i + 1):])
        # if a location was found add lower levels to existing
        else:
            loc_dict[item['Location'][i - 1]] = {**loc_dict[item['Location'][i - 1]],
                                                  **create_nested_dict(item['Location'][i:])}

    return loc_dict


#@print_func_name
def get_inven_items(inven_name, db):
    if (inven_name is None) | (inven_name == ""):
        raise PreventUpdate
    if inven_name not in db.list_collection_names():
        print("!" * 20)
        print(f"inventory ({inven_name})\nnot found - new inventory")

    inven = db[inven_name]
    items = {str(item['id']): item for item in inven.find()}

    return items

def get_item_from_id(item_id, inven_name, db):

    if item_id is None:
        raise PreventUpdate

    items = get_inven_items(inven_name, db)

    # convert item from str to dict
    item = items.get(item_id, {})
    return item


if __name__ == "__main__":

    pass
