"""
decorators for inventory
"""


def print_func_name(func):
    """this is a decorator to get the function name of what is being called
    ref: https://stackoverflow.com/questions/6200270/decorator-that-prints-function-call-details-parameters-names-and-effective-valu
    """
    def echo_func(*args, **kwargs):
        print(f"{func.__name__}")
        return func(*args, **kwargs)
    return echo_func


if __name__ == "__main__":
    pass
