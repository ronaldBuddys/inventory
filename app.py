"""
Inventory App - imports elements from multiple python modules to run
"""

import re
import json
import numpy as np
import pandas as pd

import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import dash_table

from inventory.server import app
from inventory.database import db
from inventory.utils import get_location_list, get_inven_items, get_item_from_id

from inventory.header_row import header_row, select_inventory_name
from inventory.search_tab import search_layout, update_datalist_for_suggested_inputs_based_on_inventory_name, \
    search_items, get_all_tags_from_items_to_search, change_search_location_options
from inventory.modify_tab import modify_layout, update_modify_tags

# ---
# layout
# ---

app.layout = dbc.Container(
    [
        # header row - name, inventory select and hidden divs
        *header_row,
        html.Br(),
        # html.div(id="dummy-save-div", display={})
        # tabs - one search, one add/modify/remove
        dcc.Tabs(id="tabs", value="1", children=[
            dcc.Tab(label='Search', value="1", children=search_layout, style={"color": "pink"}),
            dcc.Tab(label='Add/Modify/Remove', value="2", children=modify_layout, style={"color": "pink"})
        ])

    ]
)
app.title = "Inventory"


# ---
# callbacks
# ---

@app.callback(
    [Output('tabs', 'value'),
     Output("selected-item", "children")],
    [Input('clear-btn', "n_clicks"),
     Input('search_results_table', "derived_virtual_selected_rows")],
    [State('search_results_table', "derived_virtual_data"),
     State("selected-item", "children"),
     State("inventory-name", "value")])
def update_selected_item(clr_btn, selected_row, rows, cur_item_id, inven_name):
    """either by selecting item in search results or clearing table
    - update the selected-item
    - switch to modify tab"""

    print("-" * 50)
    print("update_selected_item")

    # --
    # inventory info
    # --

    # TODO: wrap checking and selecting items into a function
    items = get_inven_items(inven_name, db)

    cur_selected = items.get(cur_item_id, {})

    # what is driving the callback(?)
    ctx = dash.callback_context
    print(ctx.triggered)
    # clear button action:
    if ctx.triggered[0]["prop_id"].split(".")[0] == "clear-btn":
        print('clear button')
        # if current id is already null - i.e. -1 then do nothing
        # if cur_item_id == "-1":
        #     raise PreventUpdate
        return "2", "-1"

    # search data click action:
    elif ctx.triggered[0]["prop_id"].split(".")[0] == "search_results_table":
        print('search results table')
        # select the
        if selected_row == []:
            raise PreventUpdate
        out = rows[selected_row[0]]

        # don't update id (and switch to modify tab) if id is the same
        if cur_selected.get("id", None) == out['id']:
            raise PreventUpdate
        # out['Tags'] = out["Tags"].split("|")
        # out['Location'] = out["Location"].split("->")

        return "2", str(out['id'])# str(out['id'])

    else:
        raise PreventUpdate


@app.callback(
    [Output("add-mod-input-Name", "value"),
     Output("add-mod-input-Description", "value"),
     Output("add-mod-input-Comment", "value"),
     Output("add-mod-input-Quantity", "value")],
    Input("selected-item", "children"),
    State("inventory-name", "value"))
def add_selected_item_modify_inputs(item_id, inven_name):
    """given a selected item add values to modify input cells"""
    print("add_selected_item_modify_inputs")
    item = get_item_from_id(item_id, inven_name, db)

    if len(item) == {}:
        print(f"item_id: {item_id}, but was empty")

    return [item.get(i, "") for i in ["Name", "Description", "Comment", "Quantity"]]


@app.callback(
    Output("mod-id", "children"),
    Input("selected-item", "children")
)
def change_displayed_id(item_id):
    if item_id is None:
        raise PreventUpdate
    print("-" * 10)
    print("change_displayed_id")

    return f"id: {item_id}"


if __name__ == "__main__":

    app.run_server(debug=True, host='0.0.0.0', port=7000)
