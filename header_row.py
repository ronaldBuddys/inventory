"""
header - top row layout : name and inventory select
"""

import re
import json
import numpy as np
import pandas as pd

import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

from inventory.utils import get_inven_items, get_location_list
from inventory.server import app
from inventory.database import db


# --
# header layout
# --

header_row = [
    html.Div(id="selected-inventory", style={'display': 'none'}),
    # html.Div(id="items-dict", children=json.dumps(items), style={"display": "none"}),
    html.Div(id="items-location", children=json.dumps({}), style={"display": "none"}),
    html.Div(id="items-tags",
             # children=list(set([t for id, item in items.items() for t in item.get('Tags', []) ])),
             style={"display": "none"}),
    html.Div(id="selected-item", children="-1", style={"display": "none"}),
    html.Div(id="dummy", style={"display": "none"}),

    dbc.Row([
        dbc.Col(
            html.H1(id="app-name", children="No Inventory Selected", style={"color": "pink"}),
            width=6
        ),
        dbc.Col(
            [
                html.Datalist(
                    id='list-collection-names',
                    children=[html.Option(value=i) for i in db.list_collection_names()]
                ),
                dbc.Label("Name of Inventory", html_for="inventory-name"),
                dbc.Input(
                    type="text",
                    id=f"inventory-name",
                    list='list-collection-names',
                    # value="dummy",
                    placeholder=f"Enter Inventory Name",
                    debounce=True
                )
            ], width=6)
    ])

]

# --
# callbacks
# --

# select inventory name
@app.callback(
    [
        Output("app-name", "children"),
        Output("selected-inventory", "children"),
        Output("items-tags", "children"),
        Output("items-location", "children")
    ],
    Input("inventory-name", "value")
)
def select_inventory_name(inven_name):

    # TODO: if new inventory, there should be a pop up or warning: add this message

    items = get_inven_items(inven_name, db)
    print("-" * 50)
    print("select_inventory_name")

    # items-locations
    loc_dict = get_location_list(items)
    # items-tags
    tag_list = list(set([t for id, item in items.items() for t in item.get('Tags', [])]))

    return f"{inven_name} Inventory", inven_name, tag_list, json.dumps(loc_dict)


if __name__ == "__main__":

    app.layout = dbc.Container(
        [
            *header_row
        ]
    )
    app.title = "Inventory - Header Only"
    app.run_server(debug=True, host='0.0.0.0', port=7000)
