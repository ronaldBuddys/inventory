"""
mongodb database connection
"""

import pymongo
import json

from inventory import get_config_path

# mongo (config)
with open(get_config_path("mongo.json"), "r+") as f:
    mdb_cred = json.load(f)

# connect to mongodb
# TODO: add error handling around connecting to mongodb
db_url = f"mongodb://{mdb_cred['ip']}:{mdb_cred['port']}"
client = pymongo.MongoClient(db_url)

# inventory database
db = client["inventory"]


if __name__ == "__main__":

    # add 'dummy' inventory if it does not already exist

    if 'dummy' not in db.list_collection_names():
        inven = db['dummy']
        print("'dummy' inventory not found, will add")

        from inventory import get_data_path

        with open(get_data_path("dummy_inventory.json")) as f:
            d = json.load(f)

        for k, v in d.items():
            query = {key: v[key] for key in ["Name", "Description"]}
            inven.update_one(query, {"$set": v}, upsert=True)
