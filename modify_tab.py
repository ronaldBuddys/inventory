"""
layout and callback for inventory "add/modify/remove" tab
"""

import re
import json
import numpy as np
import pandas as pd

import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import dash_table

from inventory.server import app
from inventory.database import db
from inventory.utils import get_location_list, get_item_from_id, get_inven_items

# ---
# layout
# ---


mod_inputs = []
for k in ["Name", "Description", "Quantity", "Comment"]:
    tmp = [
        dbc.Label(k, html_for=f"add-mod-input-{k}"),
        dbc.Input(
            type="text",
            id=f"add-mod-input-{k}",
            placeholder=f"Enter {k}")
    ]
    mod_inputs += tmp


modify_layout = [
    html.Div(id="mod-id", children="id: -1"),
    dbc.Row([
        dbc.Col(
            dbc.Button("Save Item", id="save-item-btn", color="primary", className="mr-1"),
            width=2),
        dbc.Col(
            dbc.Button("Clear", id="clear-btn", color="primary", className="mr-1"),
            width=2)
    ]
    ),
    dbc.Row(
        dbc.Col(
            dbc.FormGroup(
                mod_inputs
            ),
            width=6,
        )
    ),
    dbc.Row(
        dbc.Col(
            # tags to
            dbc.FormGroup(
                [
                    dbc.Label("Tags", html_for=f"modify-tags"),
                    dcc.Dropdown(
                        id=f"modify-tags",
                        multi=True
                    ),
                    dbc.Input(
                        type="text",
                        id=f"modify-tags-input",
                        placeholder=f"Add a New Tag?",
                        debounce=True
                    ),
                ]
            ),
            width=6
        )
    ),
    dbc.Row(
        dbc.Col(
            dbc.FormGroup([
                dbc.Label("Location", html_for=f"add-mod-input-Location"),
                dcc.Dropdown(
                    id=f"add-mod-input-Location",
                    options=[{'label': s, 'value': s} for s in {}.items()],
                    multi=True
                ),
                dbc.Input(
                    type="text",
                    id=f"add-loc-input",
                    placeholder=f"Add Location?",
                    debounce=True
                )
            ]),
            width=6
        )
    )
]

# ---
# callbacks
# ---

@app.callback(
    [Output("modify-tags", "value"),
     Output("modify-tags", "options"),
     Output("modify-tags-input", "placeholder")
     ],
    [
        Input("selected-item", "children"),
        Input("modify-tags-input", "value")
    ],
    State("items-tags", "children"),
    State("modify-tags", "value"),
    State("modify-tags", "options"),
    State("inventory-name", "value")
)
def update_modify_tags(item_id, mod_input, all_tags, cur_tags, cur_tag_opts, inven_name):
    """given new selected item, or input to tags input then update options and values"""
    print("-" * 50)
    print("update_modify_tags")


    if mod_input is None:
        mod_input = []
    else:
        mod_input = [mod_input]
    if all_tags is None:
        all_tags = []

    if cur_tags is None:
        cur_tags = []

    cur_selected = get_item_from_id(item_id, inven_name, db)

    # get the callback_context - information on call back
    ctx = dash.callback_context
    # print(ctx.triggered)

    # TODO: this is probably a bit inefficient
    # if it was the modify-tags-input: i.e. entering text, then just use that
    if ctx.triggered[0]["prop_id"].split(".")[0] == "modify-tags-input":
        print("modify-tags-input")
        # prevent an empty tag from being added
        if mod_input[0] == "":
            raise PreventUpdate
        # output should be the current values plus the input from text
        out_vals = cur_tags + mod_input
        # the options should be all the existing plus the new one from modify tag input
        out_opts = [{"label": i, "value": i} for i in set(out_vals + all_tags)]
        return out_vals, out_opts, "Add a New Tag?"
    # otherwise it was from a new item being selected
    elif ctx.triggered[0]["prop_id"].split(".")[0] == "selected-item":
        print("selected-item")
        # if selected-item was just updated (triggered this call back) then values should just
        # - be those
        out_vals = [i for i in cur_selected.get("Tags", [])]
        # Remove any empty tags
        out_vals = [i for i in out_vals if i != '']
        # the output should be all tags
        out_opts = [{"label": i, "value": i} for i in set(out_vals + all_tags)]
        return out_vals, out_opts, "Add a New Tag?"
    else:
        print('id not understood, doing nothing')
        raise PreventUpdate


@app.callback(
    [Output("add-mod-input-Location", "value"),
     Output("add-mod-input-Location", "options")],
    [
        Input("selected-item", "children"),
        Input("add-loc-input", "value"),
        Input("clear-btn", "n_clicks"),
        Input("items-location", "children"),
    ],

    State("add-mod-input-Location", "value"),
    State("add-mod-input-Location", "options"),
    State("inventory-name", "value")
)
def update_modify_locations(item_id, mod_input, clr_btn, locs, cur_locs, cur_locs_opts, inven_name):
    """given new selected item, or input to location input then update options and values"""
    print("-" * 50)
    print("update_modify_tags")
    # cur_selected = json.loads(cur_selected)

    if mod_input is None:
        mod_input = []
    else:
        mod_input = [mod_input]
    if locs is None:
        locs = {}
    elif locs == '':
        locs = {}
    elif locs == {}:
        pass
    else:
        locs = json.loads(locs)

    if cur_locs is None:
        cur_locs = []

    cur_selected = get_item_from_id(item_id, inven_name, db)

    # get the callback_context - information on call back
    ctx = dash.callback_context


    # TODO: this is probably a bit inefficient
    # if it was the modify-tags-input: i.e. entering text, then just use that
    if ctx.triggered[0]["prop_id"].split(".")[0] == "add-loc-input":
        # prevent an empty tag from being added
        if mod_input[0] == "":
            raise PreventUpdate

        # TODO: review this logic on returning updated location
        if isinstance(locs, dict):
            # include the original values (needed to show)
            # and include the lower levels
            return cur_locs + mod_input, \
                   cur_locs_opts + [{'label': s, 'value': s} for s in locs.keys()] + [{"label": s, "value": s} for s in mod_input]
        else:
            raise PreventUpdate

        # output should be the current values plus the input from text
        out_vals = cur_locs + mod_input
        # the options should be all the existing plus the new one from modify tag input
        out_opts = [{"label": i, "value": i} for i in out_vals]
        return out_vals, out_opts

    # selected item
    elif ctx.triggered[0]["prop_id"].split(".")[0] == "selected-item":
        # output values should be those of the item
        # - locations are assumed to be ordered
        out_vals = cur_selected.get("Location", [])
        out_vals = [i for i in out_vals if i != '']

        # the options should be the below last location  (and include the current value)
        # find the last / lowest level
        for i in out_vals:
            if i in locs:
                locs = locs[i]
            else:
                break
            if len(locs) == 0:
                break
        out_opts = [{'label': i, 'value': i} for i in out_vals] + [{'label': s, 'value': s} for s in locs.keys()]
        return out_vals, out_opts
    # clear button
    elif ctx.triggered[0]["prop_id"].split(".")[0] == "clear-btn":
        # print("clear-btn NOT IMPLEMENT")
        # raise PreventUpdate
        print('clear-btn clicked: need to added the top level locations ')
        return [], [{'label': s, 'value': s} for s in locs.keys()]
    elif ctx.triggered[0]["prop_id"].split(".")[0] == "items-location":
        print("items-location NOT IMPLEMENT")
        raise PreventUpdate
        # print("items location updated")
        # # TODO: there is a fair bit of duplicate code here, mae a commenct function
        # out_vals = cur_selected.get("Location", [])
        # out_vals = [i for i in out_vals if i != '']
        #
        # # the options should be the below last location  (and include the current value)
        # # find the last / lowest level
        # for i in out_vals:
        #     if i in locs:
        #         locs = locs[i]
        #     else:
        #         break
        #     if len(locs) == 0:
        #         break
        # out_opts = [{'label': i, 'value': i} for i in out_vals] + [{'label': s, 'value': s} for s in locs.keys()]
        # return out_vals, out_opts
    else:
        print('id not understood, doing nothing')
        raise PreventUpdate

    # get the current values which are selected
    if cur_tags is None:
        cur_tags = []

    # tags from the currently selected item
    cur_sel_tags = list(set(cur_selected.get("Tags", []) + cur_tags))

    # get all the options
    all_options = [{"label": i, "value": i} for i in set(cur_sel_tags + all_tags + mod_input)]

    # new tag values: currently select items, existing on in modify tags
    cur_sel_tags

    # the tags should be the cur tags plus mod_input

    return cur_tags + mod_input, all_options


# ---
# save item
# ---
@app.callback(
    Output("dummy", "children"),
    Input("save-item-btn", "n_clicks"),
    [State("selected-item", "children"),
     State("add-mod-input-Name", "value"),
     State("add-mod-input-Description", "value"),
     State("add-mod-input-Quantity", "value"),
     State("add-mod-input-Comment", "value"),
     State("modify-tags", "value"),
     State("add-mod-input-Location", "value"),
     State("inventory-name", "value")
     ]
)
def save_item(save_btn, item_id, name, desc, quant, comm, tags, locs, inven_name):

    if save_btn is None:
        raise PreventUpdate

    print("-" * 50)
    print("save_item")

    items = get_inven_items(inven_name, db)

    if tags is None:
        tags = []
    if locs is None:
        locs = []

    # get the item
    new_item = {
        "Name": name,
        "Description": desc,
        "Comment": comm,
        "Quantity": quant,
        "Tags": tags,
        "Location": locs
    }

    if item_id == "-1":
        print("new item")

        # if all values are empty, or None, then skip
        if all([v is None for k, v in new_item.items()]):
            print("no values provided, doing nothing")
            raise PreventUpdate
        if new_item['Name'] is None:
            print("Name not provided, doing nothing")
            raise PreventUpdate

        # add an id - slow to always get the actual values
        # TODO: here just take length?

        # get new item id - this would be slow if there are lots of items
        # - would be better to store max id as hidden dev (or something)
        # new_id = max([i['id'] for i in items]) + 1
        new_id = max([int(id) for id in items.keys()]) + 1
        new_item['id'] = new_id

        query = {"id": new_item['id']}
        db[inven_name].update_one(query, {"$set": new_item}, upsert=True)

        return "Saved new item"
        # return json.dumps(items)

    else:
        print("updating existing item")
        print(f"id_val: {item_id}")
        # find the existing
        if item_id in items:
            print("id_val in items")
            new_item['id'] = int(item_id)
            # replace current value with id_val key with the 'new' / modified item
            # items[item_id] = new_item
        else:
            print("DID NOT FIND ID, DOING NOTHING")
            raise PreventUpdate

        # save item to mongodb
        query = {"id": new_item['id']}
        db[inven_name].update_one(query, {"$set": new_item}, upsert=True)

        return f"updated existing item - id:  {new_item['id']}"


    raise PreventUpdate


if __name__ == "__main__":

    from inventory.header_row import header_row, select_inventory_name

    app.layout = dbc.Container(
        [
            # header row - name, inventory select and hidden divs
            *header_row,
            # unpack list
            *modify_layout
        ]
    )
    app.title = "Inventory - Add/Modify/Remove Tab"
    app.run_server(debug=True, host='0.0.0.0', port=7000)


