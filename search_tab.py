"""
layout and callback for inventory "search" tab
"""

import re
import numpy as np
import pandas as pd

import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import dash_table

from inventory.server import app
from inventory.database import db
from inventory.utils import get_location_list

# TODO: json dictionary of items should have mongodb _id as key instead of own

# columns for search table
search_table_cols = ["Name", "Description", "Quantity", "Tags", "Location", "Comment"]

# category for each type - should get from items?
cats = {
    "Name": {"type": "text"},
    "Description": {"type": "text"},
    "Location": {"type": "text"},
    "Quantity": {"type": "number"},
    "Tags": {"type": "text"},
    "id": {"type": "number"},
    "Comment": {"type": "text"}
}

# ---
# layout
# ---

search_layout = [

    html.Br(),
    # --
    # first row of tab will be tags tat can be selected
    # --
    # dbc.Row(
    #     [
    #         # search tags
    #         dbc.Col([
    #             html.Label(id=f"search-tags-title", children="Tags", style={"color": "red"}),
    #             dcc.Dropdown(
    #                 id=f"search-tags",
    #                 placeholder="Select Tags",
    #                 multi=True
    #             )
    #         ],
    #             width={"size": 6},
    #         ),
    #         # search location
    #         dbc.Col([
    #             html.Label(id=f"search-location-title", children="Location", style={"color": "red"}),
    #             dcc.Dropdown(
    #                 id=f"search-location",
    #                 placeholder="Select Locations",
    #                 multi=True
    #             )
    #         ],
    #             width={"size": 6},
    #         )
    #     ]
    # ),
    html.Br(),
    # Search by input: Name, Description, Comment input
    dbc.Row(
        [
            dbc.Col(
                [
                    # TODO: should update children using call back (read items from html.div)
                    html.Datalist(id=f'list-suggested-inputs-{k}',
                                  # children=[html.Option(value=i)
                                  #           for i in set([item[k] for id, item in items.items()])]
                                  children=[],
                                  ),
                    html.Label(id=f"search-{k}-title", children=k, style={"color": "blue"}),
                    dbc.Input(
                        id=f"search-{k}",
                        type=cats[k]['type'],
                        list=f'list-suggested-inputs-{k}',
                        placeholder=f"Search {k}",
                        debounce=True
                    )
                ],
                width={"size": 2}, align="start")
            for k in ["Name", "Description", "Comment"]
        ] +
        [
            # search tags
            dbc.Col([
                html.Label(id=f"search-tags-title", children="Tags", style={"color": "red"}),
                dcc.Dropdown(
                    id=f"search-tags",
                    placeholder="Select Tags",
                    multi=True
                )
            ],
                width={"size": 3},
            ),
            # search location
            dbc.Col([
                html.Label(id=f"search-location-title", children="Location", style={"color": "red"}),
                dcc.Dropdown(
                    id=f"search-location",
                    placeholder="Select Locations",
                    multi=True
                )
            ],
                width={"size": 3},
            )
        ]
    ),
    # Search Results (table)
    # TODO: columns and data should come from callback
    html.Div(id="search_results",
             children=[
                 dash_table.DataTable(
                     id='search_results_table',
                     # TODO: change this to be populated by a call back
                     columns=[{"name": i, "id": i} for i in search_table_cols],
                     # columns=[{"name": i, "id": i} for i in items[0].keys()],
                     data=[],
                     # row_selectable='single',
                     row_selectable='single',
                     sort_action='native',
                     # elected_row_indices=[],
                     style_cell={
                         'whiteSpace': 'normal',
                         'height': 'auto',
                     }
                 )
             ], style={"margin-top": "45px"})
]

# ---
# callbacks
# ---



# update DataList depending on the inventory selected
# TODO: this should update automatically if a new object is added
# - will require interaction with Modify Tab
@app.callback(
    [Output(f"list-suggested-inputs-{k}", "children") for k in ["Name", "Description", "Comment"]],
    [Input("inventory-name", "value"),
     Input("dummy", "children")]
)
def update_datalist_for_suggested_inputs_based_on_inventory_name(inven_name, dummy_div):

    if (inven_name is None) | (inven_name == ""):
        raise PreventUpdate

    # search from database
    inven = db[inven_name]

    # TODO: handle this being empty
    items = {str(i): item for i, item in enumerate(inven.find())}
    if len(items) == 0:
        print(f"there were no items found in: {inven_name}")

    # get the unique (set) of options for each category
    out = [
        [html.Option(value=i) for i in set([item[k] for id, item in items.items()])]
        for k in ["Name", "Description", "Comment"]
    ]
    return out


@app.callback(
    Output('search_results_table', 'data'),
    [Input('search-tags', 'value'),
     Input('search-location', 'value'),
     Input('search-Name', "value"),
     Input('search-Description', "value"),
     Input('search-Comment', "value")],
    State("inventory-name", "value"))
def search_items(search_tags, search_locs, search_names, search_desc, search_com, inven_name):

    # TODO: in search_items when searching for locations take values that match location (list)
    # - in specified order - is this too strict?
    # TODO: consider if all search items are empty then return an empty table?

    if (inven_name is None) | (inven_name == ""):
        raise PreventUpdate

    if search_tags is None:
        search_tags = []
    if search_locs is None:
        search_locs = []

    # TODO: should the default be show everything?
    print("-" * 50)
    print("search_items")

    # search from database
    inven = db[inven_name]

    # TODO: allow for the 'dummy' collection to be missing, read data from package instead
    items = {str(i): item for i, item in enumerate(inven.find())}

    # items = json.loads(items)

    # HARDCODED: search logic - match all or any (effectively AND or OR)
    search_logic = "AND"
    and_or = np.all if search_logic == "AND" else np.any

    # elements - atomic values to search for
    element_search = {}
    if len(search_tags):
        element_search["Tags"] = search_tags
    if len(search_locs):
        element_search["Location"] = search_locs

    # text to search
    text_search = {
        "Name": search_names,
        "Description": search_desc,
        "Comment": search_com
    }

    # for each item see if ANY search tag is in the item tag then select that item to return
    search_res = []
    for id, item in items.items():
        # check if elements are found
        element_find = [and_or(np.in1d(v, item[k]))
                        for k, v in element_search.items()]
        # check if texts are found
        text_find = [bool(re.search(v, item[k], re.IGNORECASE))
                     for k, v in text_search.items() if v is not None]

        # if any or all are found, depending on search_logic, then add item
        if and_or(element_find + text_find):
            search_res += [item]

    # format values in items that are list for table output
    for i in range(len(search_res)):
        item = search_res[i]
        item["Tags"] = "|".join(item["Tags"])
        item["Location"] = "->".join(item["Location"])
        item.pop("_id")
        search_res[i] = item

    return search_res


@app.callback(
    Output("search-tags", "options"),
    Input("inventory-name", "value"))
def get_all_tags_from_items_to_search(inven_name):

    if (inven_name is None) | (inven_name == ""):
        raise PreventUpdate

    print("-" * 50)
    print("get_all_tags_from_items_to_search")

    # search from database
    inven = db[inven_name]

    # TODO: handle this being empty
    items = {str(i): item for i, item in enumerate(inven.find())}

    # for each item, if there are "tags", get each. take set to get unique (slow?) then convert to list
    out = set([t for _, i in items.items() for t in i.get('Tags', [])])
    out = [{"label": i, "value": i} for i in out]
    return out


@app.callback(
    Output('search-location', 'options'),
    [
        Input('search-location', 'value'),
        Input("inventory-name", "value")
    ])
def change_search_location_options(v, inven_name):
    """given the current location(s) selected (could be empty)
     and the items-location (nested dict)
     - update the search location options to get the options for the lower levels of search-location value"""

    if v is None:
        v = []

    if (inven_name is None) | (inven_name == ""):
        raise PreventUpdate

    print("-")
    print("change_search_location_options")

    # search from database
    inven = db[inven_name]

    # TODO: handle this being empty
    items = {str(i): item for i, item in enumerate(inven.find())}

    locs = get_location_list(items)

    # for each element in the location list find in the locations dict
    # ASSUMES v will be order correctly
    for i in v:
        if i in locs:
            locs = locs[i]
        # if locs is None:
        if len(locs) == 0:
            break

    print(locs)

    if isinstance(locs, dict):
        # include the orignal values (needed to show)
        # and include the lower levels
        return [{'label': i, 'value': i} for i in v] + [{'label': s, 'value': s} for s in locs.keys()]
    else:
        raise PreventUpdate


if __name__ == "__main__":

    from inventory.header_row import header_row, select_inventory_name

    app.layout = dbc.Container(
        [
            *header_row,
            # unpack search layout list
            *search_layout
        ]
    )
    app.title = "Inventory - Search Tab"
    app.run_server(debug=True, host='0.0.0.0', port=7000)



